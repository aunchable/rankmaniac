import subprocess
import time

t1 = time.time()
i = 0
while True:
	outputfile = ""
	print i
	if i == 0:
		# Dataset selection
		# subprocess.call("python pagerank_map_weighted.py < ../local_test_data/GNPn100p05 | sort | python pagerank_reduce_weighted.py | python process_map_weighted.py  | sort| python process_reduce_weighted.py > output_weighted.txt", shell=True)
		subprocess.call("python pagerank_map_weighted.py < ../local_test_data/EmailEnron  | sort | python pagerank_reduce_weighted.py | python process_map_weighted.py  | sort | python process_reduce_weighted.py > output_weighted.txt", shell=True)
		# subprocess.call("python pagerank_map.py < /Users/surajnair/Downloads/parsedStanfordData.txt | sort | python pagerank_reduce.py | python process_map.py  | sort | python process_reduce.py > output.txt", shell=True)
		outputfile = "output_weighted"
	elif i % 2 == 0:
		# Alternate output between two files.
		subprocess.call("python pagerank_map_weighted.py < input_weighted.txt  | sort | python pagerank_reduce_weighted.py | python process_map_weighted.py | sort | python process_reduce_weighted.py > output_weighted.txt", shell=True)
		outputfile = "output_weighted"
	else:
		subprocess.call("python pagerank_map_weighted.py < output_weighted.txt  | sort | python pagerank_reduce_weighted.py | python process_map_weighted.py | sort | python process_reduce_weighted.py > input_weighted.txt", shell=True)
		outputfile = "input_weighted"
	i += 1

	# In the case that the output file is a list of FinaLRanks
	# we know that the MapReduce has finished
	with open(outputfile + ".txt") as f:
		s = f.read()
		if "FinalRank" in s:
			break
t2 = time.time()
print "Done", t2-t1
print "Results in " + outputfile
