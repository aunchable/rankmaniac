#!/usr/bin/env python

import sys

# Function for writing out information for a particular node.
def write_out(curr_key, curr_key_new_pr, curr_key_inlinks, curr_rest, send_inlinks):
    if send_inlinks:
        sys.stdout.write("A" + "\t" + 'm' + str(curr_key) + ',' + str(curr_key_inlinks) + '\n')
        sys.stdout.write("B" + "\t" + 'm' + str(curr_key) + ',' + str(curr_key_inlinks) + '\n')
    sys.stdout.write("A" + "\t" + 'i' + str(curr_key) + ',' +
        str(curr_key_new_pr) + ',' + curr_rest + "\n")
    sys.stdout.write("B" + "\t" + 'i' + str(curr_key) + ',' +
        str(curr_key_new_pr) + ',' + curr_rest + "\n")

# Variable initialization
curr_key = -1
curr_key_new_pr = 0.15    # (1 - alpha), alpha = 0.85
curr_key_inlinks = 0
curr_rest = ''
send_inlinks = False

for line in sys.stdin:
    # Mapper key,value pair splitter
    (key, value) = line.strip().split('\t')

    if curr_key == -1:
        curr_key = key

    # A new key has been reached in the collected file
    if curr_key != key:
        write_out(curr_key, curr_key_new_pr, curr_key_inlinks, curr_rest, send_inlinks)
        curr_key = key
        curr_key_new_pr = 0.15    # (1 - alpha), alpha = 0.85
        curr_key_inlinks = 0
        curr_rest = ''
        send_inlinks = False

    # Perform the proper variable assignment based on the type of
    # information presented for the key
    if value[0] == 'r':
        curr_key_new_pr += 0.85 * float(value[1:])
    elif value[0] == 'i':
        curr_rest = value[1:]
    else:  # value[0] == 'm':
        curr_key_inlinks += int(value[1:])
        send_inlinks = True
    # elif value[0] == 'p':
    #     curr_key_pr = float(value[1:].split(',')[0])
    # else:  # value[0] == 'i'
    #     curr_iter = int(value[1:])

write_out(curr_key, curr_key_new_pr, curr_key_inlinks, curr_rest, send_inlinks)
