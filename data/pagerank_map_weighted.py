#!/usr/bin/env python

# import re
import sys

#
# This program takes an input line and sends information about itself and
# new PageRank value components to the reducer
#

local_reduce = {}
inlinks = {}

for line in sys.stdin:

    # Empty line check
    if not line:
        continue

    # Parse data
    if line[0] == "N":
        spt = line[7:].strip()
        part1,part2 = spt.split("\t")
        part2 = part2.split(",")
        nbrs = part2[2:]
        data = (part1, "0", part2[0], part2[1], nbrs)
        # Determine the in-link size of each of the neighbors connected via outlink
        # from the current node.
        for n in data[4]:
            if int(n) not in inlinks:
                inlinks[int(n)] = 1
            else:
                inlinks[int(n)] += 1

    else:
        # On first iteration initialize PR to degree
        sp = line.strip().split(",")
        data = (sp[0], sp[1], sp[2], sp[3], sp[4:])
        # sys.stdout.write(str(int(n)) + '\tm' + str(data[0]) + '!' + str(len(nbrs)) + '\n')

    # Assign PageRanks
    # no neighbors:
    if len(data[4]) == 0:

        if int(data[0]) not in local_reduce:
            # sys.stderr.write(data[0], " ")
            local_reduce[int(data[0])] = float(data[2])
        else:
            # sys.stderr.write(data[0])
            local_reduce[int(data[0])] += float(data[2])
        #sys.stdout.write(str(int(data[0])) + '\tr' + data[2] + '\n')
    elif data[1] == '0':
        pr = float(data[2]) / float(len(data[4]))
        for n in data[4]:
            if int(n) not in local_reduce:
                local_reduce[int(n)] = pr
            else:
                local_reduce[int(n)] += pr
    else:
        node_inlinks = []
        for n in data[4]:
            (node, il) = n.split('!')
            node_inlinks.append([int(node), int(il)])
        total_inlinks = sum([ni[1] for ni in node_inlinks])
        for ni in node_inlinks:
            # Split PR of node among its neighbors according to their
            # in-degree ratios
            pr = float(data[2]) * float(ni[1]) / float(total_inlinks)
            if int(ni[0]) not in local_reduce:
                local_reduce[int(ni[0])] = pr
            else:
                local_reduce[int(ni[0])] += pr
            #sys.stdout.write(str(ni[0]) + '\tr' + pr + '\n')
            #sys.stdout.write(str(int(n)) + '\tr' + pr + '\n')

    # Send info once
    if len(data[4]) == 0:
        sys.stdout.write(str(data[0]) + '\ti' + data[1] + ',' + data[2] +
            ',' + data[3] + '\n')
    else:
        sys.stdout.write(str(data[0]) + '\ti' + data[1] + ',' + data[2] +
            ',' + data[3] + ',' + ','.join(data[4]) +'\n')


for k, v in local_reduce.items():
    sys.stdout.write(str(k) + '\tr' + str(v) + '\n')

if len(inlinks) > 0:
    for k, v in inlinks.items():
        sys.stdout.write(str(k) + '\tm' + str(v) + '\n')
