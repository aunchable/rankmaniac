#!/usr/bin/env python

from operator import itemgetter
# import re
import sys

# NUM_ITERS = 15
# epsilon = 0.01

diffs = [] # IF WE WANT TO USE DIFFS IN PR
sum_curr_prs = 0.0 # FOR NORMALIZATION
sum_prev_prs = 0.0 # FOR NORMALIZATION

top20 = {} # CURRENT
top20_old = {} # PREVIOUS

iter_num = -1

min_node = None
min_pr = -1.0

K = 15

min_node_old = None
min_pr_old = -1.0

curr_run = None
run_num = None

DONE = False

# FIRST READS ALL DATA INTO LIST
for line in sys.stdin:
    ipt = line.strip().split("\t")
    p1 = ipt[0]
    ipt = ipt[1].split(",")
    (key, iteration, curr_pr, prev_pr, prevprev_pr, neighbors) = (p1, ipt[0], ipt[1], ipt[2], ipt[3], ipt[4:])

    # print(key, iteration, curr_pr, prev_pr, prevprev_pr, neighbors)
    # print(top20)

    num_iters = int(iteration)

    if num_iters < K:
        if len(neighbors) == 0:
            sys.stdout.write(
                key + ',' + str(int(iteration)+1) + ',' +
                curr_pr + ',' + prev_pr + '\n')
        else:
            sys.stdout.write(
                key + ',' + str(int(iteration)+1) + ',' +
                curr_pr + ',' + prev_pr + ',' + ','.join(neighbors) + '\n')

    else:
        top20[key] = float(curr_pr)


if len(top20) > 0:
    new_sorted = sorted(top20.items(), key=itemgetter(1))
    for i in range(20):
        sys.stdout.write('FinalRank:' + str(new_sorted[len(new_sorted) - 1-i][1]) + '\t' + new_sorted[len(new_sorted) - 1-i][0] + '\n')
