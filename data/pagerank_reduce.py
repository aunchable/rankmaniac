#!/usr/bin/env python

import sys

# Function for writing out information for a particular node.
def write_out(curr_key, curr_key_old_pr, curr_key_pr, curr_key_new_pr, curr_key_n, curr_iter):
    if curr_key_n == '':
        sys.stdout.write(
            str(curr_key) + '\t' + str(curr_iter) + ',' +
            str(curr_key_new_pr) + ',' + str(curr_key_pr) + ',' + str(curr_key_old_pr) + "\n")
    else:
        sys.stdout.write(
            str(curr_key) + '\t' + str(curr_iter) + ',' +
            str(curr_key_new_pr) + ',' + str(curr_key_pr) + ',' + str(curr_key_old_pr) + ',' +
            curr_key_n + "\n")

# Variable initialization
curr_key = -1
curr_key_pr = 0.0
curr_key_old_pr = 0.0
curr_key_new_pr = 0.15    # (1 - alpha), alpha = 0.85
curr_key_n = ''
curr_iter = 0


for line in sys.stdin:
    # Mapper key,value pair splitter
    (key, value) = line.strip().split('\t')

    if curr_key == -1:
        curr_key = key

    # A new key has been reached in the collected file
    if curr_key != key:
        write_out(curr_key, curr_key_old_pr, curr_key_pr, curr_key_new_pr, curr_key_n, curr_iter)
        curr_key = key
        curr_key_pr = 0.0
        curr_key_old_pr = 0.0
        curr_key_new_pr = 0.15    # (1 - alpha), alpha = 0.85
        curr_key_n = ''
        curr_iter = 0

    # Perform the proper variable assignment based on the type of
    # information presented for the key
    if value[0] == 'r':
        curr_key_new_pr += 0.85 * float(value[1:])
    elif value[0] == 'n':
        curr_key_n = value[1:]
    elif value[0] == 'p':
        curr_key_pr = float(value[1:].split(',')[0])
        curr_key_old_pr = float(value[1:].split(',')[1])
    else:  # value[0] == 'i'
        curr_iter = int(value[1:])


write_out(curr_key, curr_key_old_pr, curr_key_pr, curr_key_new_pr, curr_key_n, curr_iter)
