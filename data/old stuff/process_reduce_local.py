#!/usr/bin/env python

from operator import itemgetter
import re
import sys

NUM_ITERS = 20
input_data_pattern = re.compile('(\d*)\t(\d+),(\d*.\d*),(\d*.\d*),(.*)')

def find_min(top20):
    curr_min_node = None
    curr_min_pr = None

    for node, pr in top20.iteritems():
        if curr_min_node == None:
            curr_min_node = node
            curr_min_pr = pr
        elif curr_min_pr > pr:
            curr_min_node = node
            curr_min_pr = pr

    # min_node = curr_min_node
    # min_pr = curr_min_pr
    return [curr_min_node, curr_min_pr]

def add_to_top20(top20, node, pr, min_node, min_pr):
    if len(top20) < 20:
        if min_node == '' or pr < min_pr:
            min_pr = pr
            min_node = node
        top20[node] = pr

    else:

        if pr < min_pr:
            del top20[min_node]
            top20[node] = pr
            #set_min()

def run(inputs):
    top20 = {} # moved this here
    min_node = '' # moved this here
    min_pr = -1.0 # moved this here

    result = []
    for line in inputs:

        #print 'LINE'
        #print line
        (key, iteration, curr_pr, prev_pr, neighbors) = input_data_pattern.match(line).groups()
        #print (key, iteration, curr_pr, prev_pr, neighbors)

        if int(iteration) == NUM_ITERS - 1:
            # print 'ITERATIONS DONE'
            # a = find_min(top20)
            # print 'min top 20'
            # print a
            # print (key, iteration, curr_pr, prev_pr, neighbors)
            top20[key] = curr_pr
            # add_to_top20(top20, key, curr_pr, a[0], a[1])

        else:
            if neighbors == '':
                # sys.stdout.write(
                #     'NodeId:' + key + ',' + iteration + '\t' +
                #     curr_pr + ',' + prev_pr)
                result.append('NodeId:' + key + ',' + str(int(iteration) + 1) + '\t' + curr_pr + ',' + prev_pr)
            else:
                # sys.stdout.write(
                #     'NodeId:' + key + ',' + iteration + '\t' +
                #     curr_pr + ',' + prev_pr + ',' + neighbors)
                result.append('NodeId:' + key + ',' + str(int(iteration) + 1) + '\t' + curr_pr + ',' + prev_pr + ',' + neighbors)

    if len(top20) > 0:
        top20 = sorted(top20.items(), key=itemgetter(1))
        for i in range(20):
            #sys.stdout.write('FinalRank:' + str(top20[19-i][1]) + '\t' + top20[19-i][0])
            result.append('FinalRank:' + str(top20[len(top20) - 1 -i][1]) + '\t' + top20[len(top20) - 1-i][0])

    # print "DONE"
    # print result
    return result
