#!/usr/bin/env python

import sys


def write_out(curr_key, curr_key_pr, curr_key_new_pr, curr_key_n, curr_iter):
    return (
        str(curr_key) + '\t' + str(curr_iter) + ',' +
        str(curr_key_new_pr) + ',' + str(curr_key_pr) + ',' +
        curr_key_n)

def run(inputs):

    curr_key = -1
    curr_key_pr = 0.0
    curr_key_new_pr = 0.15    # (1 - alpha), alpha = 0.85
    curr_key_n = ''
    curr_iter = 0

    result = []

    for line in inputs:

        (key, value) = line.split('\t')

        if curr_key == -1:
            curr_key = key

        if curr_key != key:
            result.append(write_out(curr_key, curr_key_pr, curr_key_new_pr, curr_key_n, curr_iter))
            # write_out(curr_key, curr_key_pr, curr_key_new_pr, curr_key_n, curr_iter)
            curr_key = key
            curr_key_pr = 0.0
            curr_key_new_pr = 0.15    # (1 - alpha), alpha = 0.85
            curr_key_n = ''
            curr_iter = 0

        if value[0] == 'r':
            curr_key_new_pr += 0.85 * float(value[1:])
        elif value[0] == 'n':
            curr_key_n = value[1:]
        elif value[0] == 'p':
            curr_key_pr = float(value[1:].split(',')[0])
        else:  # value[0] == 'i'
            curr_iter = int(value[1:])

    result.append(write_out(curr_key, curr_key_pr, curr_key_new_pr, curr_key_n, curr_iter))
    return result
