#!/usr/bin/env python

import sys

#
# This program simply represents the identity function.
#

def run(inputs):

	results = []
	for line in inputs:
	    #sys.stdout.write(line)
		results.append(line)

	return results
