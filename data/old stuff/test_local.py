import pagerank_map_local
import pagerank_reduce_local
import process_map_local
import process_reduce_local

import sys

INPUT_FILEPATH = '../local_test_data/EmailEnron'
SOLS_FILEPATH = '../sols/EmailEnron'

NUM_ITERS = 50

def test_full():

    print 'started'

    with open(INPUT_FILEPATH) as input_file:
        input_list = list(input_file)
        for i in range(NUM_ITERS):
            print "Iteration: " + str(i)
            input_list = list(pagerank_map_local.run(input_list))

            input_list.sort()
            # print(input_list)
            # print("FINISHED PAGERANK MAP")
            # break

            # import pagerank_reduce
            input_list = list(pagerank_reduce_local.run(input_list))
            #print input_list
            # print("FINISHED PAGERANK REDUCE")

            input_list = list(process_map_local.run(input_list))
            # print("FINISHED PROCESS MAP")
            input_list.sort()
            input_list = list(process_reduce_local.run(input_list))
            # print("FINISHED PROCESS REDUCE")

            if input_list[0].startswith('FinalRank:'):
                break

    # print ''.join(input_list)
    print '\nPage Ranks Found\n'
    page_ranks_found = []

    for i in range(len(input_list)):
        page_ranks_found.append( int(  input_list[i].split("\t")[1] )  )

    print page_ranks_found

    print '\nActual Page Ranks\n'

    actual_page_ranks = []

    with open(SOLS_FILEPATH) as sol_file:
        solution_list = list(sol_file)
        #print int(''.join(solution_list).split("\n") )
        actual_page_ranks.append (  ''.join(solution_list).split("\n")   )

    actual_page_ranks =  actual_page_ranks[0][0: 20]
    actual_page_ranks = map(int, actual_page_ranks)
    print actual_page_ranks

test_full()
