#!/usr/bin/env python

import re
import sys

#
# This program takes an input line and sends information about itself and
# new PageRank value components to the reducer
#

input_data_pattern = re.compile('NodeId:(\d*),?(\d*)\t(\d*.\d*),(\d*.\d*),?(.*)')

def run(inputs):
    result =[]
    for line in inputs:

        # Empty line check
        if not line:
            continue

        # Parse data
        split = input_data_pattern.match(line).groups()
        data = (split[0], split[1], split[2], split[3], split[4].split(','))
        if not data[1]:
            data = (split[0], "0", split[2], split[3], split[4].split(','))

        # data: (ID, iteration#, curr PageRank, prev PageRank, list of neighbors)

        # Assign PageRanks
        # no neighbors:
        if data[4][0] == '':
            result.append( (str(data[0]) + '\tr' + str(float(data[2])) + '\n') )
            # sys.stdout.write(str(data[0]) + '\tr' + str(float(data[2])) + '\n')
        else:
            pr = str(float(data[2]) / float(len(data[4])))
            for n in data[4]:
                result.append(n + '\tr' + pr + '\n')
                # sys.stdout.write(n + '\tr' + pr + '\n')

        # Send other information
        # Neighbors:
        if data[4][0] != '':
            result.append(str(data[0]) + '\tn' + ','.join(data[4]) + '\n')
            # sys.stdout.write(str(data[0]) + '\tn' + ','.join(data[4]) + '\n')
        # Curr/prev pageranks:
        result.append(str(data[0]) + '\tp' + data[2] + ',' + data[3] + '\n')
        # sys.stdout.write(str(data[0]) + '\tp' + data[2] + ',' + data[3] + '\n')
        # iteration
        result.append(str(data[0]) + '\ti' + data[1] + '\n')
        # sys.stdout.write(str(data[0]) + '\ti' + data[1] + '\n')

    return result
