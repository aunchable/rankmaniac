import subprocess
import time


# NUM_ITERS = 50

t1 = time.time()
i = 0
while True:
	outputfile = ""
	print i
	if i == 0:
		# subprocess.call("python pagerank_map.py < ../local_test_data/GNPn100p05 | sort | python pagerank_reduce.py | python process_map.py  | sort| python process_reduce.py > output.txt", shell=True)
		# subprocess.call("python pagerank_map.py < ../local_test_data/EmailEnron | sort | python pagerank_reduce.py | python process_map.py | sort | python process_reduce.py > output.txt", shell=True)
		subprocess.call("python pagerank_map.py < /Users/surajnair/Downloads/parsedStanfordData.txt | sort | python pagerank_reduce.py | python process_map.py  | sort | python process_reduce.py > output.txt", shell=True)
		outputfile = "output"
	elif i % 2 == 0:
		subprocess.call("python pagerank_map.py < output2.txt  | sort | python pagerank_reduce.py | python process_map.py | sort | python process_reduce.py > output.txt", shell=True)
		outputfile = "output"
	else:
		subprocess.call("python pagerank_map.py < output.txt | sort | python pagerank_reduce.py | python process_map.py | sort | python process_reduce.py > output2.txt", shell=True)
		outputfile = "output2"
	i += 1
	t2 = time.time()
	print "Time", t2-t1
	with open(outputfile + ".txt") as f:
		s = f.read()
		if "FinalRank" in s:
			break
t2 = time.time()
print "Done", t2-t1
print "Results in " + outputfile