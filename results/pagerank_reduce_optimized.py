#!/usr/bin/env python

import sys


def write_out(curr_key, curr_key_old_pr, curr_key_pr, curr_key_new_pr, curr_key_n, curr_iter, curr_rest):
    sys.stdout.write("A" + "\t" + str(curr_key) + ',' +
        str(curr_key_new_pr) + ',' + curr_rest + "\n")
    sys.stdout.write("B" + "\t" + str(curr_key) + ',' +
        str(curr_key_new_pr) + ',' + curr_rest + "\n")
    # if curr_key_n == '':
    #     sys.stdout.write("A" + "\t" + str(curr_key) + ',' +
    #         str(curr_key_new_pr) + ',' + curr_rest + "\n")
    #     sys.stdout.write("A" + "\t" +
    #         str(curr_key) + ',' + str(curr_iter) + ',' +
    #         str(curr_key_new_pr) + ',' + str(curr_key_pr) + ',' + str(curr_key_old_pr) + "\n")
    #     sys.stdout.write("B" + "\t" +
    #         str(curr_key) + ',' + str(curr_iter) + ',' +
    #         str(curr_key_new_pr) + ',' + str(curr_key_pr) + ',' + str(curr_key_old_pr) + "\n")
    # else:
    #     sys.stdout.write("A" + "\t" +
    #         str(curr_key) + ',' + str(curr_iter) + ',' +
    #         str(curr_key_new_pr) + ',' + str(curr_key_pr) + ',' + str(curr_key_old_pr) + ',' +
    #         curr_key_n + "\n")
    #     sys.stdout.write("B" + "\t" +
    #         str(curr_key) + ',' + str(curr_iter) + ',' +
    #         str(curr_key_new_pr) + ',' + str(curr_key_pr) + ',' + str(curr_key_old_pr) + ',' +
    #         curr_key_n + "\n")

curr_key = -1
curr_key_pr = 0.0
curr_key_old_pr = 0.0
curr_key_new_pr = 0.15    # (1 - alpha), alpha = 0.85
curr_key_n = ''
curr_iter = 0
curr_rest = ''


for line in sys.stdin:

    (key, value) = line.strip().split('\t')

    if curr_key == -1:
        curr_key = key

    if curr_key != key:
        write_out(curr_key, curr_key_old_pr, curr_key_pr, curr_key_new_pr, curr_key_n, curr_iter, curr_rest)
        curr_key = key
        curr_key_pr = 0.0
        curr_key_old_pr = 0.0
        curr_key_new_pr = 0.15    # (1 - alpha), alpha = 0.85
        curr_key_n = ''
        curr_iter = 0
        curr_rest = ''

    if value[0] == 'r':
        curr_key_new_pr += 0.85 * float(value[1:])
#    elif value[0] == 'n':
#        curr_key_n = value[1:]
#    elif value[0] == 'p':
#        curr_key_pr = float(value[1:].split(',')[0])
#        curr_key_old_pr = float(value[1:].split(',')[1])
    else:  # value[0] == 'i'
        curr_rest = value[1:]
        #curr_iter = int(value[1:])


write_out(curr_key, curr_key_old_pr, curr_key_pr, curr_key_new_pr, curr_key_n, curr_iter, curr_rest)
