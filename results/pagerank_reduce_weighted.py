#!/usr/bin/env python

import sys


def write_out(curr_key, curr_key_pr, curr_key_new_pr, curr_key_n, curr_key_n_list, curr_iter):
    if len(curr_key_n_list) == 0:
        if curr_key_n == '':
            sys.stdout.write(
                str(curr_key) + '\t' + str(curr_iter) + ',' +
                str(curr_key_new_pr) + ',' + str(curr_key_pr) + "\n")
        else:
            sys.stdout.write(
                str(curr_key) + '\t' + str(curr_iter) + ',' +
                str(curr_key_new_pr) + ',' + str(curr_key_pr) + ',' +
                curr_key_n + "\n")
    else:
        sys.stdout.write(
            str(curr_key) + '\t' + str(curr_iter) + ',' +
            str(curr_key_new_pr) + ',' + str(curr_key_pr) + ',' +
            ','.join(curr_key_n_list) + "\n")

curr_key = -1
curr_key_pr = 0.0
curr_key_new_pr = 0.15    # (1 - alpha), alpha = 0.85
curr_key_n = ''
curr_key_n_list = []
curr_iter = 0

for line in sys.stdin:

    (key, value) = line.strip().split('\t')

    if curr_key == -1:
        curr_key = key

    if curr_key != key:
        write_out(curr_key, curr_key_pr, curr_key_new_pr, curr_key_n, curr_key_n_list, curr_iter)
        curr_key = key
        curr_key_pr = 0.0
        curr_key_new_pr = 0.15    # (1 - alpha), alpha = 0.85
        curr_key_n = ''
        curr_key_n_list = []
        curr_iter = 0

    if value[0] == 'r':
        curr_key_new_pr += 0.85 * float(value[1:])
    elif value[0] == 'n':
        curr_key_n = value[1:]
    elif value[0] == 'm':
        curr_key_n_list.append(value[1:])
    elif value[0] == 'p':
        curr_key_pr = float(value[1:].split(',')[0])
    else:  # value[0] == 'i'
        curr_iter = int(value[1:])

write_out(curr_key, curr_key_pr, curr_key_new_pr, curr_key_n, curr_key_n_list, curr_iter)
