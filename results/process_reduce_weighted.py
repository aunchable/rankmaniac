#!/usr/bin/env python

from operator import itemgetter
# import re
import sys

# NUM_ITERS = 15
# epsilon = 0.01

diffs = [] # IF WE WANT TO USE DIFFS IN PR
sum_curr_prs = 0.0 # FOR NORMALIZATION
sum_prev_prs = 0.0 # FOR NORMALIZATION

top20 = {} # CURRENT
top_20_old = {} # PREVIOUS

min_node = ''
min_pr = -1.0
iter_num = -1

# FIRST READS ALL DATA INTO LIST
rows = []
for line in sys.stdin:

    # PARSE INPUT
    ipt = line.strip().replace("\t", ",").split(",")
    (key, iteration, curr_pr, prev_pr, neighbors) = (ipt[0], ipt[1], ipt[2], ipt[3], ipt[4:])

    # SAVES DATA / STATISTICS FOR NORMALIZATION / ITERATIONS NUMBER
    rows.append((key, iteration, curr_pr, prev_pr, neighbors))
    diffs.append(abs(float(curr_pr) - float(prev_pr)))
    top20[key] = curr_pr
    sum_curr_prs += float(curr_pr)
    top_20_old[key] = prev_pr
    sum_prev_prs += float(prev_pr)
    iter_num = int(iteration)

# SORTS BASED ON RANK
new_sorted = sorted(top20.items(), key=itemgetter(1))
old_sorted = sorted(top_20_old.items(), key=itemgetter(1))

# IF WE WANT TO USE THE DROPOUT METHOD
# if iter_num > 20:
#     bottom20p = new_sorted[:int(len(new_sorted) * (0.01 * iter_num))]
#     bottom20p_keys = [int(i[0]) for i in bottom20p]
# else:
#     bottom20p_keys = []

# PULLS OUT TOP 20 TO CHECK STOPPING CONDITION
top20 = new_sorted[-50:]
top_20_old = old_sorted[-50:]
top_20_keys = [int(i[0]) for i in top20]
top_20_old_keys = [int(i[0]) for i in top_20_old]
# WHILE TOP 50 ARE STILL CHANGING, KEEP RUNNING
if top_20_keys != top_20_old_keys:
    for row in rows:
        (key, iteration, curr_pr, prev_pr, neighbors) = row

        if iter_num >= 0:
            for i in range(len(neighbors)):
                node = neighbors[i].split('!')[0]
                neighbors[i] = str(node) + '!1'

        # IF WE WANT TO USE DROPOUT METHOD
        # if int(key) not in bottom20p_keys:

        # NORMALIZE
        curr_pr = str(float(curr_pr) / (sum_curr_prs / len(rows)))
        prev_pr = str(float(prev_pr) / (sum_prev_prs / len(rows)))

        # WRITEOUT
        if len(neighbors)==0:
            sys.stdout.write(
                'NodeId:' + key + ',' + str(int(iteration)+1) + '\t' +
                curr_pr + ',' + prev_pr + '\n')
        else:
            sys.stdout.write(
                'NodeId:' + key + ',' + str(int(iteration)+1) + '\t' +
                curr_pr + ',' + prev_pr + ',' + ','.join(neighbors) + '\n')
else:
    for i in range(20):
        sys.stdout.write('FinalRank:' + str(top20[len(top20) - 1-i][1]) + '\t' + top20[len(top20) - 1-i][0] + '\n')
