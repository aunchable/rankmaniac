#!/usr/bin/env python

# import re
import sys

#
# This program takes an input line and sends information about itself and
# new PageRank value components to the reducer
#

for line in sys.stdin:

    # Empty line check
    if not line:
        continue

    # Parse data
    spt = (line.strip().split(":"))[1].split("\t")
    part2 = spt[1].split(",")
    part1 = spt[0].split(",")
    cP, pP = part2[:2]
    nbrs = part2[2:]
    if len(part1) > 1:
        data = (part1[0], part1[1], part2[0], part2[1], nbrs)
    else:
        # data = (part1[0], "0", part2[0], part2[1], nbrs)
        # On first iteration initialize PR to degree
        data = (part1[0], "0", str(1.0 * len(nbrs)), part2[1], nbrs)
        for n in data[4]:
            sys.stdout.write(str(int(n)) + '\tm' + str(data[0]) + '!' + str(len(nbrs)) + '\n')

    # Assign PageRanks
    # no neighbors:
    if len(data[4]) == 0:
        sys.stdout.write(str(int(data[0])) + '\tr' + data[2] + '\n')
    elif data[1] == '0':
        pr = str(float(data[2]) / float(len(data[4])))
        for n in data[4]:
            sys.stdout.write(str(int(n)) + '\tr' + pr + '\n')
    else:
        node_inlinks = []
        for n in data[4]:
            (node, inlinks) = n.split('!')
            node_inlinks.append([int(node), int(inlinks)])
        total_inlinks = sum([ni[1] for ni in node_inlinks])
        for ni in node_inlinks:
            pr = str(float(data[2]) * float(ni[1]) / float(total_inlinks))
            sys.stdout.write(str(ni[0]) + '\tr' + pr + '\n')

    # Send other information
    # Neighbors:
    if len(data[4]) != 0:
        sys.stdout.write(str(data[0]) + '\tn' + ','.join(data[4]) + '\n')
    # Curr/prev pageranks:
    sys.stdout.write(str(data[0]) + '\tp' + data[2] + ',' + data[3] + '\n')
    # iteration
    sys.stdout.write(str(data[0]) + '\ti' + data[1] + '\n')
