#!/usr/bin/env python

# import re
import sys

#
# This program takes an input line and sends information about itself and
# new PageRank value components to the reducer
#

local_reduce = {}

for line in sys.stdin:

	# Empty line check
	if not line:
		continue

	# Parse data
	if line[0] == "N":
		spt = line[7:].strip()
		part1,part2 = spt.split("\t")
		part2 = part2.split(",")
		nbrs = part2[2:]
		# data = (part1, "0", str(1.0 * len(nbrs)), part2[1], nbrs)
		data = (part1, "0", part2[0], part2[1], nbrs)
	else:
		# data = (part1[0], "0", part2[0], part2[1], nbrs)
		# On first iteration initialize PR to degree
		sp = line.strip().split(",")
		data = (sp[0], sp[1], sp[2], sp[3], sp[4:])

	# print("*******", data)

	# Assign PageRanks
	# no neighbors:
	if len(data[4]) == 0:
		
		if int(data[0]) not in local_reduce:
			# sys.stderr.write(data[0], " ")
			local_reduce[int(data[0])] = float(data[2])
		else:
			# sys.stderr.write(data[0])
			local_reduce[int(data[0])] += float(data[2])
		#sys.stdout.write(str(int(data[0])) + '\tr' + data[2] + '\n')
	else:
		pr = float(data[2]) / float(len(data[4]))
		for n in data[4]:
			if int(n) not in local_reduce:
				local_reduce[int(n)] = pr
			else:
				local_reduce[int(n)] += pr
			#sys.stdout.write(str(int(n)) + '\tr' + pr + '\n')

	# Send other information
	# Neighbors:
	if len(data[4]) != 0:
		sys.stdout.write(str(data[0]) + '\tn' + ','.join(data[4]) + "\n")
	# Curr/prev pageranks:
	sys.stdout.write(str(data[0]) + '\tp' + data[2] + ',' + data[3] + '\n')
	# iteration
	sys.stdout.write(str(data[0]) + '\ti' + data[1] + '\n')

for k, v in local_reduce.items():
	sys.stdout.write(str(k) + '\tr' + str(v) + '\n')
