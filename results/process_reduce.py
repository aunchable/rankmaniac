#!/usr/bin/env python

from operator import itemgetter
# import re
import sys

# NUM_ITERS = 15
# epsilon = 0.01

diffs = [] # IF WE WANT TO USE DIFFS IN PR
sum_curr_prs = 0.0 # FOR NORMALIZATION
sum_prev_prs = 0.0 # FOR NORMALIZATION

top20 = {} # CURRENT
top20_old = {} # PREVIOUS

iter_num = -1

min_node = None
min_pr = -1.0

K = 15

min_node_old = None
min_pr_old = -1.0

curr_run = None
run_num = None

DONE = False


# def set_min():
#     global min_node, min_pr
#     curr_min_node = None
#     curr_min_pr = None

#     for node, pr in top20.iteritems():
#         if curr_min_node == None:
#             curr_min_node = node
#             curr_min_pr = pr
#         elif curr_min_pr > pr:
#             curr_min_node = node
#             curr_min_pr = pr

#     min_node = curr_min_node
#     min_pr = curr_min_pr


# def add_to_top20(node, pr):
#     global min_node, min_pr, top20
#     if len(top20) < K:
#         if min_node is None or pr < min_pr:
#             min_pr = pr
#             min_node = node
#         top20[node] = pr
#     else:
#         if pr > min_pr:
#             del top20[min_node]
#             top20[node] = pr
#             set_min()

# def set_min_old():
#     global min_node_old, min_pr_old
#     curr_min_node = None
#     curr_min_pr = None

#     for node, pr in top20_old.iteritems():
#         if curr_min_node == None:
#             curr_min_node = node
#             curr_min_pr = pr
#         elif curr_min_pr > pr:
#             curr_min_node = node
#             curr_min_pr = pr

#     min_node_old = curr_min_node
#     min_pr_old = curr_min_pr


# def add_to_top20_old(node, pr):
#     global min_node_old, min_pr_old, top20_old
#     if len(top20_old) < K:
#         if min_node_old is None or pr < min_pr_old:
#             min_pr_old = pr
#             min_node_old = node
#         top20_old[node] = pr
#     else:
#         if pr > min_pr_old:
#             del top20_old[min_node_old]
#             top20_old[node] = pr
#             set_min_old()

# def top20change(top_20_old_keys, top_20_keys):
#     avg_change = 0.0
#     max_change = 0

#     # Iterate through the current list and determine the distance moved for that
#     # node. Assume that nodes not in the previous list (but present in current
#     # list) moved from the 21st position.
#     for i in range(len(top_20_keys)):
#         node = top_20_keys[i]

#         past_index = 20
#         for j in range(len(top_20_old_keys)):
#             if top_20_old_keys[j] == node:
#                 past_index = j

#         change = (i - past_index)**2
#         avg_change += change
#         max_change = max(max_change, (i - past_index)**2)

#     # For those that are in the past list and not in the current list, we assume
#     # that they must have fallen out of their position down to the 21st position.
#     not_shared = 0
#     for i in range(len(top_20_old_keys)):
#         if top_20_old_keys[i] not in top_20_keys:
#             change = (20 - i)**2
#             avg_change += change
#             max_change = max(max_change, (i - past_index)**2)
#             not_shared += 1
#     avg_change /= not_shared + 20

#     return (avg_change, max_change)

# FIRST READS ALL DATA INTO LIST
for line in sys.stdin:

    # PARSE INPUT
    # ipt = line[2:].strip().split(",")
    # (key, iteration, curr_pr, prev_pr, prevprev_pr, neighbors) = (ipt[0], ipt[1], ipt[2], ipt[3], ipt[4], ipt[5:])

    ipt = line.strip().split("\t")
    p1 = ipt[0]
    ipt = ipt[1].split(",")
    (key, iteration, curr_pr, prev_pr, prevprev_pr, neighbors) = (p1, ipt[0], ipt[1], ipt[2], ipt[3], ipt[4:])

    # print(key, iteration, curr_pr, prev_pr, prevprev_pr, neighbors)
    # print(top20)

    num_iters = int(iteration)

    if num_iters < K:
        if len(neighbors) == 0:
            sys.stdout.write(
                key + ',' + str(int(iteration)+1) + ',' +
                curr_pr + ',' + prev_pr + '\n')
        else:
            sys.stdout.write(
                key + ',' + str(int(iteration)+1) + ',' +
                curr_pr + ',' + prev_pr + ',' + ','.join(neighbors) + '\n')

    else:
        top20[key] = float(curr_pr)


if len(top20) > 0:
    new_sorted = sorted(top20.items(), key=itemgetter(1))
    for i in range(20):
        sys.stdout.write('FinalRank:' + str(new_sorted[len(new_sorted) - 1-i][1]) + '\t' + new_sorted[len(new_sorted) - 1-i][0] + '\n')


    # if curr_run is None:
    #     curr_run = line[0]
    #     run_num = True
    # elif line[0] != curr_run:
    #     curr_run = line[0]
    #     run_num = False

    #     new_sorted = sorted(top20.items(), key=itemgetter(1))
    #     old_sorted = sorted(top20_old.items(), key=itemgetter(1))
    #     top20_keys = [int(i[0]) for i in new_sorted[-K:]]
    #     top20_old_keys = [int(i[0]) for i in old_sorted[-K:]]


    #     if top20_keys == top20_old_keys:
    #         for i in range(20):
    #             sys.stdout.write('FinalRank:' + str(new_sorted[len(new_sorted) - 1-i][1]) + '\t' + new_sorted[len(new_sorted) - 1-i][0] + '\n')
    #         DONE = True

    # if not DONE:
    #     if run_num:

    #         # SAVES DATA / STATISTICS FOR NORMALIZATION / ITERATIONS NUMBER
    #         add_to_top20(key, curr_pr)
    #         add_to_top20_old(key, prev_pr)


    #     else:
    #         if len(neighbors)==0:
    #             sys.stdout.write(
    #                 key + ',' + str(int(iteration)+1) + ',' +
    #                 curr_pr + ',' + prev_pr + '\n')
    #         else:
    #             sys.stdout.write(
    #                 key + ',' + str(int(iteration)+1) + ',' +
    #                 curr_pr + ',' + prev_pr + ',' + ','.join(neighbors) + '\n')




    # momentum = 

    # diffs.append(abs(float(curr_pr) - float(prev_pr)))
    # top20[key] = curr_pr
    # sum_curr_prs += float(curr_pr)
    # top_20_old[key] = prev_pr
    # sum_prev_prs += float(prev_pr)
    # iter_num = int(iteration)

# SORTS BASED ON RANK
# new_sorted = sorted(top20.items(), key=itemgetter(1))
# old_sorted = sorted(top_20_old.items(), key=itemgetter(1))

# IF WE WANT TO USE THE DROPOUT METHOD
# if iter_num > 20:
#     bottom20p = new_sorted[:int(len(new_sorted) * (0.01 * iter_num))]
#     bottom20p_keys = [int(i[0]) for i in bottom20p]
# else:
#     bottom20p_keys = []

# PULLS OUT TOP 20 TO CHECK STOPPING CONDITION
# top20 = new_sorted[-50:]
# top_20_old = old_sorted[-50:]
# top_20_keys = [int(i[0]) for i in top20]
# top_20_old_keys = [int(i[0]) for i in top_20_old]

# avg_change, max_change = top20change(top_20_old_keys, top_20_keys)
# # WHILE TOP 50 ARE STILL CHANGING, KEEP RUNNING
# if top_20_keys != top_20_old_keys:
#     for row in rows:
#         (key, iteration, curr_pr, prev_pr, neighbors) = row


#         # IF WE WANT TO USE DROPOUT METHOD
#         # if int(key) not in bottom20p_keys:

#         # NORMALIZE
#         curr_pr = str(float(curr_pr) / (sum_curr_prs / len(rows)))
#         prev_pr = str(float(prev_pr) / (sum_prev_prs / len(rows)))

#         # WRITEOUT
#         if len(neighbors)==0:
#             sys.stdout.write(
#                 key + ',' + str(int(iteration)+1) + ',' +
#                 curr_pr + ',' + prev_pr + '\n')
#         else:
#             sys.stdout.write(
#                 key + ',' + str(int(iteration)+1) + ',' +
#                 curr_pr + ',' + prev_pr + ',' + ','.join(neighbors) + '\n')
# else:
#     for i in range(20):
#         sys.stdout.write('FinalRank:' + str(top20[len(top20) - 1-i][1]) + '\t' + top20[len(top20) - 1-i][0] + '\n')

# # sys.stdout.write('Current Top 20: \t' + str(top_20_keys) + '\n')
# # sys.stdout.write('Past Top 20: \t\t' + str(top_20_old_keys) + '\n')
# # sys.stdout.write('Average: ' + str(avg_change) + ' Max:' + str(max_change))
